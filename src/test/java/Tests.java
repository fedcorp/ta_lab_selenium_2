import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.Assert;

public class Tests implements Locators {

    private WebDriver driver;
    private String log;
    private String pw;
    private WebElement element;

    @BeforeTest
    public void precondition(){
        try {
            log = "ta.lab.testprofile@gmail.com";
            pw = "ownerofthisPROFILE";
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver_win32/chromedriver.exe");
            driver = new ChromeDriver();
        }catch (SecurityException e) {
            e.printStackTrace();
        }
    }
    @AfterTest
    public void atTheEnd(){
        driver.quit();
    }
    @Test
    public void googleLogin(){
        driver.get("https://www.gmail.com/");

        element = driver.findElement(By.cssSelector(INPUT_EMAIL_FORM_CSS));
        element.sendKeys(log);
        element = driver.findElement(By.cssSelector(NEXT_AFTER_EMAIL_FORM_CSS));
        element.click();

        new WebDriverWait(driver, 30).until(d -> d.findElement(By.xpath(WAIT_AFTER_EMAIL_INPUT_XPATH)).getText().equalsIgnoreCase("ta.lab.testprofile@gmail.com"));

        element = driver.findElement(By.cssSelector(INPUT_PW_FORM_CSS));
        element.sendKeys(pw);
        element = driver.findElement(By.cssSelector(NEXT_AFTER_PW_FORM_CSS));
        element.click();

        Assert.assertTrue(new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(BUTTON_WRITE_EMAIL_XPATH))).isDisplayed());
    }

    @Test(dependsOnMethods="googleLogin")
    public void sendEmail(){

        element = driver.findElement(By.xpath(BUTTON_WRITE_EMAIL_XPATH));
        element.click();

        element = driver.findElement(By.xpath(INPUT_EMAIL_RECEIVER_XPATH));
        element.sendKeys("evilzluj@mail.ru");

        element = driver.findElement(By.xpath(INPUT_EMAIL_TITLE_XPATH));
        element.sendKeys("WebDriver Test");

        element = driver.findElement(By.xpath(INPUT_EMAIL_MESSAGE_XPATH));
        element.sendKeys("Test message from Chrome WebDriver");

        element = driver.findElement(By.xpath(BUTTON_SEND_EMAIL_XPATH));
        element.click();

        //Depends on gmail account language! Works on Ukrainian.
        Assert.assertTrue(new WebDriverWait(driver, 20).until(d -> d.findElement(By.xpath(IS_EMAIL_SENT_XPATH)).getText().equalsIgnoreCase("лист надіслано.")));
    }
}
