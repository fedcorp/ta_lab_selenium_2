public interface Locators {

    //CSS locators
    String INPUT_EMAIL_FORM_CSS = "input[type='email']";
    String NEXT_AFTER_EMAIL_FORM_CSS = "div#identifierNext";
    String INPUT_PW_FORM_CSS = "input[name='password']";
    String NEXT_AFTER_PW_FORM_CSS = "div#passwordNext";

    //XPath locators
    String WAIT_AFTER_EMAIL_INPUT_XPATH = "//div[@id='profileIdentifier']";
    String BUTTON_WRITE_EMAIL_XPATH = "//div[@class='z0']/div[@role='button']";
    String INPUT_EMAIL_RECEIVER_XPATH = "//form[@class='bAs']//textarea[@name='to']";
    String INPUT_EMAIL_TITLE_XPATH = "//form/div/input[@name='subjectbox']";
    String INPUT_EMAIL_MESSAGE_XPATH = "//div[@class='AD']//td/div/div[@role='textbox']";
    String BUTTON_SEND_EMAIL_XPATH = "//div[@class='AD']//tr[@class='btC']/td/div/div[@role='button']";
    String IS_EMAIL_SENT_XPATH = "//span[@class='bAq']";
}
